<?php

/**
 * @file
 * Defines the API functions provided by the gsmi module.
 */

/**
 * Implements hook_gsmi_imagelayout().
 *
 * @see https://www.php.net/manual/en/ref.image.php
 */
function hook_gsmi_imagelayout($settings) {
  /*
   * Create a new GD image stream (required)
   * https://www.php.net/manual/de/function.imagecreatetruecolor.php
   */
  $newimage = @imagecreatetruecolor($settings['width'], $settings['height'])
    or die("Can't create GD image stream");

  /*
   * Example: Print dark text on bright background image (optional)
   * https://www.php.net/manual/de/function.imagettftext.php
   */
  $background_color = imagecolorallocate($newimage, 240, 240, 240);
  imagefill($newimage, 0, 0, $background_color);
  $text_color = imagecolorallocate($newimage, 10, 10, 10);
  $text = print_r($settings, TRUE);
  $fontfile = $settings['font'];
  imagettftext($newimage, 12, 0, 50, 50, $text_color, $fontfile, $text);

  // Return GD image Stream (required).
  return $newimage;
}
