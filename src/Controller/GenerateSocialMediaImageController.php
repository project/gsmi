<?php

namespace Drupal\gsmi\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Returns responses for Generate Social Media Image routes.
 */
class GenerateSocialMediaImageController extends ControllerBase {

  /**
   * Generate image.
   *
   * Ex : /generate-social-media-image/savetofile/11/true,
   * Ex : /generate-social-media-image/returnasimage/11.
   *
   * @param int $nid
   *   Image nid.
   * @param boolean $save_as_media_image
   *   If to save as media image.
   */

  /**
   * Builds the response.
   */
  public function saveToFile($nid, $save_to_media) {

    $settings = _gsmi_buildsettings($nid);
    $image = _gsmi_templatebuilder($settings);

    // https://riptutorial.com/php/example/18401/image-output
    $file_name = "generated_" . uniqid();

    if ($settings['image-type'] == 'image/png') {
      $file_extension = ".png";
      $quality = 9;
      $uri = 'public://generate/' . $file_name . $file_extension;
      imagepng($image, $uri, $quality);
    }
    else {
      $file_extension = ".jpg";
      $quality = 90;
      $uri = 'public://generate/' . $file_name . $file_extension;
      imagejpeg($image, $uri, $quality);
    }
    imagedestroy($image);

    // https://www.drupal8.ovh/en/tutoriels/47/create-a-file-drupal-8.
    $image_file = File::create([
      'uid' => $this->currentUser->id(),
      'filename' => $file_name . $file_extension,
      'uri' => $uri,
      'status' => 1,
    ]);
    $image_file->save();

    $response = '';
    if ($save_to_media) {
      // https://gist.github.com/steffenr/a40bab1f3b1c066d5c0655351e2107fd.
      $image_media = Media::create([
        'bundle' => 'image',
        'uid' => $this->currentUser->id(),
        'langcode' => $this->languageManager->getDefaultLanguage()->getId(),
        'status' => 1,
        "field_media_image" => [
          "target_id" => $image_file->id(),
          "alt" => $settings['text'],
        ],
      ]);
      $image_media->save();
      $response .= '<br>File saved to <a href="/media/' . $image_media->id() . '/edit" target="_blank">media system</a>.';
    }

    $response = '<br>File <i>' . $file_name . $file_extension . '</i> saved to <a href="/admin/content/files" target="_blank">file system</a>.</br>';

    $response .= '<img src="/sites/default/files/generate/' . $file_name . $file_extension . '" width="1200" height="630"/></br>';

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $response,
    ];

    return $build;
  }

  /**
   * Returns a node as an image.
   */
  public function returnAsImage($nid) {

    $settings = _gsmi_buildsettings($nid);
    $image = _gsmi_templatebuilder($settings);

    imageinterlace($image, TRUE);

    $headers = [
      "Cache-Control" => "no-store, no-cache, must-revalidate, max-age=0",
    ];

    if ($settings['image-type'] == 'image/png') {
      $headers = ["Content-type" => 'image/png'];
      $response = new StreamedResponse(
      function () use ($image) {
        imagepng($image, NULL, 0);
        imagedestroy($image);
      }, 200, $headers);
    }
    else {
      $headers = ["Content-type" => 'image/jpeg'];
      $response = new StreamedResponse(
        function () use ($image) {
          imagejpeg($image, NULL, 90);
          imagedestroy($image);
        }, 200, $headers);
    }

    $response->send();
  }

}

/**
 * Builds the settings.
 */
function _gsmi_buildsettings($nid) {
  $config = \Drupal::config('gsmi.settings');
  $text = '';
  $node = [];
  // https://blog.42mate.com/how-to-do-some-basic-code-things-in-drupal-8/
  if ($nid) {
    $node = Node::load($nid);
  }
  if ($node) {
    $text = $node->get($config->get('text-field'))->getString();
  }
  else {
    // Get Website Name as fallback.
    $text = \Drupal::config('system.site')->get('name');
  }
  $fid = '';
  $imageurl = '';
  $imagefield = explode('.', $config->get('image-field'));
  $mid = $config->get('fallback-image');

  if ($node && !empty($imagefield[0])) {
    // Get file-ID of image depending on field-type (image or entity-reference).
    if ($imagefield[0] == 'imagefield') {
      $fid = $node->get($imagefield[1])->target_id;
    }
    elseif ($imagefield[0] == 'mediafield') {
      $referencedEntityID = $node
        ->get($imagefield[1])
        ->first();
      if ($referencedEntityID) {
        $referencedEntity = $referencedEntityID
          ->get('entity')
          ->getTarget()
          ->getValue();
        $fid = $referencedEntity->getSource()->getSourceFieldValue($referencedEntity);
      }
    }
  }

  if (!$fid && $mid) {
    // Use Fallback Image if there is no image in associated fields.
    $media = Media::load($mid);
    $fid = $media->getSource()->getSourceFieldValue($media);
  }

  if ($fid) {
    // Get image url out of fid.
    $imagefile = File::load($fid);
    if (empty($config->get('image-style'))) {
      $imageurl = $imagefile->createFileUrl();
    }
    else {
      $style = ImageStyle::load($config->get('image-style'));
      $imageurl = $style->buildUrl($imagefile->uri->value);
    }
  }

  if ($config->get('font')) {
    // Get url to font out of file-id.
    $fontfile = File::load($config->get('font'))->getFileUri();
  }
  else {
    // Use fallback font.
    $fontfile = \Drupal::service('extension.list.module')->getPath("gsmi") . '/font/Ubuntu-Bold.ttf';
  }
  $fontfile_path = \Drupal::service('file_system')->realpath($fontfile);

  // Convert percentage of image to pixel in image.
  $gradientstart = '';
  $gradientend = '';
  if ($config->get('gradient-direction') == 'vertical') {
    $gradientstart = $config->get('gradient-start') * $config->get('height') / 100;
    $gradientend = $config->get('gradient-end') * $config->get('height') / 100;
  }
  else {
    $gradientstart = $config->get('gradient-start') * $config->get('width') / 100;
    $gradientend = $config->get('gradient-end') * $config->get('width') / 100;
  }

  $target_image_type = $config->get('image-type');
  if ($config->get('image-type') == 'auto') {
    // Set to image type of background-image or fallback to JPEG.
    $target_image_type = ($imageurl) ? image_type_to_mime_type(exif_imagetype($imageurl)) : 'image/jpeg';
  }

  $settings = [
    'nid' => $nid,
    'text' => $text,
    'imageurl' => $imageurl,
    'bg-color' => $config->get('bg-color'),
    'gradient-color' => $config->get('gradient-color'),
    'gradient-start' => $gradientstart,
    'gradient-end' => $gradientend,
    'gradient-direction' => $config->get('gradient-direction'),
    'text-color' => $config->get('text-color'),
    'font' => $fontfile_path,
    'font-size' => $config->get('font-size'),
    'width' => $config->get('width'),
    'height' => $config->get('height'),
    'logo' => $config->get('logo'),
    'image-type' => $target_image_type,
    'quality' => $config->get('quality'),
    'template' => $config->get('template'),
  ];

  return $settings;
}
