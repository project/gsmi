<?php

namespace Drupal\gsmi\Form;

use Drupal\Component\Utility\Color;
use Drupal\Core\Database\Database;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;

/**
 * Configure Generate Social Media Image settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gsmi_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['gsmi.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('gsmi.settings');

    $form['content'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Content'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    // Get list of entities with mapped fields.
    $field_map = \Drupal::service('entity_field.manager')->getFieldMap();
    // Get list of node-fields of type "String".
    $stringfield = array_filter($field_map['node'], function ($var) {
      return ($var['type'] == 'string');
    });
    // Build options for select-list.
    foreach (array_keys($stringfield) as $key => $value) {
      $textfields[$value] = $value;
    }
    $form['content']['text-field'] = [
      '#type' => 'select',
      '#title' => $this->t('Source of text'),
      '#default_value' => $config->get('text-field'),
      '#description' => t('Select a textfield as source for the text to display. The nodes title will be the fallback, if the selected field is empty or does not exist.'),
      '#options' => $textfields,
    ];

    $fid = $config->get('fallback-image');
    $file = (isset($fid) ? Media::load($fid) : NULL);
    $form['content']['fallback-image'] = [
      '#type' => 'entity_autocomplete',
      '#target_type'   => 'media',
      '#title' => $this->t('Fallback Image'),
      '#description' => t('To use as a fallback-image. File formats: ".png" or ".jpg". Recommended file-size: 1200x630px'),
      '#default_value' => $file,
    ];

    $imagefield = array_filter($field_map['node'], function ($var) {
      return ($var['type'] == 'image');

    });
    foreach (array_keys($imagefield) as $key => $value) {
      $imagefields['imagefield.' . $value] = $value;
    }
    $mediafield = array_filter($field_map['node'], function ($var) {
      return ($var['type'] == 'entity_reference');
    });
    foreach (array_keys($mediafield) as $key => $value) {
      if (str_starts_with($value, 'field_')) {
        if (FieldStorageConfig::loadByName('node', $value)->getSetting('target_type') == 'media') {
          $imagefields['mediafield.' . $value] = $value;
        };
      }
    }
    $form['content']['image-field'] = [
      '#type' => 'select',
      '#title' => $this->t('Source of image'),
      '#default_value' => $config->get('image-field'),
      '#description' => t('Select an image-field (not "entity reference") as source for an image to display. Or select none to always use fallback image.'),
      '#options' => $imagefields,
      '#empty_option' => '',
    ];

    $styles = ImageStyle::loadMultiple();
    foreach ($styles as $key => $value) {
      $image_styles[$key] = $value->label();
    }
    $form['content']['image-style'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Image Style'),
      '#description' => t('Select Image Style for image to be placed.'),
      '#default_value' => $config->get('image-style'),
      '#options' => $image_styles,
    ];

    $form['design'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Design'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['design']['text-color'] = [
      '#type' => 'color',
      '#title' => $this->t('Text-Color'),
      '#default_value' => $config->get('text-color'),
      '#description' => t('A hexadecimal string for the color without a leading "#". May use the shorthand notation (e.g., "123" for "112233")'),
    ];
    $form['design']['gradient-color'] = [
      '#type' => 'color',
      '#title' => $this->t('Gradient-Color'),
      '#default_value' => $config->get('gradient-color'),
      '#description' => t('A hexadecimal string for the gradient-color without a leading "#". May use the shorthand notation (e.g., "123" for "112233"). Leave empty for no gradient.'),
    ];
    $form['design']['gradient-start'] = [
      '#type' => 'number',
      '#title' => $this->t('Start Gradient'),
      '#size' => 3,
      '#maxlength' => 3,
      '#min' => 0,
      '#max' => 100,
      '#field_suffix' => '%',
      '#default_value' => $config->get('gradient-start'),
      '#description' => t('Where should the gradient start?'),
    ];
    $form['design']['gradient-end'] = [
      '#type' => 'number',
      '#title' => $this->t('End Gradient'),
      '#size' => 3,
      '#maxlength' => 3,
      '#min' => 0,
      '#max' => 100,
      '#field_suffix' => '%',
      '#default_value' => $config->get('gradient-end'),
      '#description' => t('Where should the gradient end?'),
    ];
    $form['design']['gradient-direction'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Image Style'),
      '#description' => t('Select the direction of the gradient.'),
      '#default_value' => $config->get('gradient-direction'),
      '#options' => [
        'horizontal' => t('horizontal'),
        'vertical' => t('vertical'),
      ],
    ];
    $form['design']['bg-color'] = [
      '#type' => 'color',
      '#title' => $this->t('Background-Color'),
      '#default_value' => $config->get('bg-color'),
      '#description' => t('A hexadecimal string for the background-color without a leading "#". May use the shorthand notation (e.g., "123" for "112233"). This color is used, if no background-image provided.'),
    ];

    // https://www.drupal8.ovh/en/tutoriels/159/database-basic-examples
    $con = Database::getConnection();
    $search_phrase = '.ttf';
    $query = $con->select('file_managed', 'f')
      ->fields('f', ['fid', 'filename', 'filemime'])
      ->condition('filename', '%' . $con->escapeLike($search_phrase), 'LIKE')
      ->condition('filemime', "application/octet-stream", '=')
      ->execute();
    $result = $query->fetchAll();
    $fontfiles = [];
    foreach ($result as $key => $value) {
      $fontfiles[$value->fid] = $value->filename;
    }
    $form['design']['font'] = [
      '#type' => 'select',
      '#title' => $this->t('Font File'),
      '#description' => t('The uploaded TrueType font you wish to use. Select none for fallback-font (Ubuntu-Bold.ttf).'),
      '#default_value' => $config->get('font'),
      '#empty_option' => '',
      '#options' => $fontfiles,
    ];
    $form['design']['fontupload'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Upload Font File'),
      '#upload_location' => 'public://generate/assets',
      '#description' => t('The TrueType font you wish to use. File format: ".ttf". Will be saved in public://generate/assets and overwrite the setting below.'),
      '#upload_validators' => [
        'file_validate_extensions' => ['ttf'],
      ],
    ];
    $form['design']['font-size'] = [
      '#type' => 'number',
      '#size' => 2,
      '#min' => 3,
      '#max' => 99,
      '#title' => $this->t('Font size'),
      '#field_suffix' => 'px',
      '#default_value' => $config->get('font-size'),
      '#description' => t('The font-size in pixels, e.g. 40'),
    ];

    $defaulttheme = \Drupal::config('system.theme')->get('default');
    $defaultlogopath = theme_get_setting('logo', $defaulttheme);

    if ($config->get('logo') !== NULL) {
      $defaultlogopath = $config->get('logo');
    }
    else {
      $defaulttheme = \Drupal::config('system.theme')->get('default');
      $defaultlogo = theme_get_setting('logo', $defaulttheme);
      $defaultlogopath = '';
      if (@is_array(getimagesize($defaultlogo['url'])) && exif_imagetype($defaultlogo['url']) == IMAGETYPE_PNG) {
        $defaultlogopath = $defaultlogo['url'];
      }
    }

    $form['design']['logo'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to logo'),
      '#default_value' => $defaultlogopath,
      '#description' => t('Absolute path to custom logo like "https://domain.com/path/to/logo.png". Must be a file of type PNG.'),
    ];

    // Information about modules' implementations of the template
    // HOOK_gsmi_imagelayout().
    $templates = $this->getImageLayoutImplementations();

    $form['design']['template'] = [
      '#type' => 'select',
      '#title' => $this->t('Templates'),
      '#description' => t('Select the available template provided by modules'),
      '#options' => $templates,
      '#default_value' => $config->get('template'),
      '#access' => count($templates) > 1,
    ];

    $form['output'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Output'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['output']['width'] = [
      '#type' => 'number',
      '#size' => 4,
      '#min' => 80,
      '#max' => 4096,
      '#title' => $this->t('The width of the image in pixels.'),
      '#field_suffix' => 'px',
      '#default_value' => $config->get('width'),
      '#description' => t('Recommended: 1200px'),
    ];
    $form['output']['height'] = [
      '#type' => 'number',
      '#size' => 4,
      '#min' => 80,
      '#max' => 4096,
      '#title' => $this->t('The height of the image in pixels.'),
      '#field_suffix' => 'px',
      '#default_value' => $config->get('height'),
      '#description' => t('Recommended: 630px'),
    ];
    $form['output']['image-type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the type of the generated image-file'),
      '#description' => t('"Auto" takes the same image-type as the source-image.'),
      '#default_value' => $config->get('image-type'),
      '#options' => [
        'auto' => $this->t('Auto'),
        'image/jpeg' => $this->t('JPEG'),
        'image/png' => $this->t('PNG'),
      ],
    ];
    $form['output']['quality'] = [
      '#type' => 'number',
      '#size' => 3,
      '#min' => 0,
      '#max' => 100,
      '#title' => $this->t('JPEG quality'),
      '#field_suffix' => '%',
      '#default_value' => $config->get('quality'),
      '#description' => t('Define the image quality for JPEG manipulations. Ranges from 0 to 100. Higher values mean better image quality but bigger files.'),
    ];

    $form['preview'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Preview'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 900,
    ];
    $nid = $config->get('preview-node');
    $previewnode = (isset($nid) ? Node::load($nid) : NULL);
    $form['preview']['preview-node'] = [
      '#type' => 'entity_autocomplete',
      '#target_type'   => 'node',
      '#title' => $this->t('Title of the node to preview'),
      '#description' => t('Save configuration to preview oder save image below'),
      '#default_value' => $previewnode,
    ];
    $markup = '<div id="preview"><img loading="lazy" src="/generate-social-media-image/returnasimage/' . $config->get('preview-node') . '" /><br><a href="/generate-social-media-image/savetofile/' . $config->get('preview-node') . '">Save as file</a><br><a href="/generate-social-media-image/savetofile/' . $config->get('preview-node') . '/true">Save as media-file</a></div>';

    $form['preview']['markup'] = [
      '#markup' => $markup,
      '#access' => $nid > 0,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $textcolor = $form_state->getValue('text-color');
    if (Color::validateHex($textcolor) === FALSE && !empty($textcolor)) {
      $form_state->setErrorByName('text-color', $this->t('The color-value for the text is not correct.'));
    }
    $backgroundcolor = $form_state->getValue('bg-color');
    if (Color::validateHex($backgroundcolor) === FALSE && !empty($backgroundcolor)) {
      $form_state->setErrorByName('bg-color', $this->t('The color-value for the background is not correct.'));
    }
    $gradientcolor = $form_state->getValue('gradient-color');
    if (Color::validateHex($gradientcolor) === FALSE && !empty($gradientcolor)) {
      $form_state->setErrorByName('gradient-color', $this->t('The color-value for the gradient is not correct.'));
    }
    if (((float) $form_state->getValue('gradient-end') - (float) $form_state->getValue('gradient-start')) < 0) {
      $form_state->setErrorByName('gradient-end', $this->t('The end-value must be higher than the start-value.'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $fontfid = NULL;
    $form_file = $form_state->getValue('fontupload', 0);

    if (isset($form_file[0]) && !empty($form_file[0])) {
      $fontfile = File::load($form_file[0]);
      $fontfile->setPermanent();
      $fontfile->save();
      $fontfid = $fontfile->id();
    }
    $font = ($fontfid) ? $fontfid : $form_state->getValue('font');
    $textcolor = (empty($form_state->getValue('text-color'))) ? '#000000' : $form_state->getValue('text-color');
    $bgcolor = (empty($form_state->getValue('bg-color'))) ? '#ffffff' : $form_state->getValue('bg-color');
    $gradientcolor = (empty($form_state->getValue('gradient-color'))) ? '#ffffff' : $form_state->getValue('gradient-color');
    $gradientstart = (empty($form_state->getValue('gradient-start'))) ? '30' : $form_state->getValue('gradient-start');
    $gradientend = (empty($form_state->getValue('gradient-end'))) ? '60' : $form_state->getValue('gradient-end');
    $fontsize = (empty($form_state->getValue('font-size'))) ? '40' : $form_state->getValue('font-size');
    $width = (empty($form_state->getValue('width'))) ? '1200' : $form_state->getValue('width');
    $height = (empty($form_state->getValue('height'))) ? '630' : $form_state->getValue('height');
    $quality = (empty($form_state->getValue('quality'))) ? '80' : $form_state->getValue('quality');
    $template = (count($this->getImageLayoutImplementations()) == 1) ? 0 : $form_state->getValue('template');

    $this->config('gsmi.settings')
      ->set('text-field', $form_state->getValue('text-field'))
      ->set('image-field', $form_state->getValue('image-field'))
      ->set('image-style', $form_state->getValue('image-style'))
      ->set('text-color', $textcolor)
      ->set('bg-color', $bgcolor)
      ->set('gradient-color', $gradientcolor)
      ->set('gradient-start', $gradientstart)
      ->set('gradient-end', $gradientend)
      ->set('gradient-direction', $form_state->getValue('gradient-direction'))
      ->set('font', $font)
      ->set('font-size', $fontsize)
      ->set('template', $template)
      ->set('logo', $form_state->getValue('logo'))
      ->set('width', $width)
      ->set('height', $height)
      ->set('image-type', $form_state->getValue('image-type'))
      ->set('quality', $quality)
      ->set('fallback-image', $form_state->getValue('fallback-image'))
      ->set('preview-node', $form_state->getValue('preview-node'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Returns a list of modules that implement the HOOK_gsmi_imagelayout().
   *
   * This maintains compatibility with Drupal < 9.4
   */
  private function getImageLayoutImplementations(): array {
    $implementations = [];
    if (method_exists(\Drupal::moduleHandler(), 'invokeAllWith')) {
      \Drupal::moduleHandler()->invokeAllWith(
        'gsmi_imagelayout',
        function (callable $hook, string $module) use (&$implementations) {
          $implementations[] = $module;
        }
      );
    }
    else {
      // Use the deprecated getImplementations() for Drupal < 9.4.
      $implementations = \Drupal::moduleHandler()->getImplementations('gsmi_imagelayout');
    }
    return $implementations;
  }

}
